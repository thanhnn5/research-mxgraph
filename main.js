function main(container) {
    // Checks if the browser is supported
    if (!mxClient.isBrowserSupported()) {
        mxUtils.error('Browser is not supported!', 200, false);
    } else {
        var id = 1;
        var btn_type = 'primary';
        var cell_selected = null;

        var cell_button_list = [];
        var cell_card_list = [];
        // Creates the graph inside the given container

        var graph = new mxGraph(container);
        new mxRubberband(graph);
        var keyHandler = new mxKeyHandler(graph);

        keyHandler.bindKey(46, function (evt) {
            graph.removeCells();
            cell_button_list.remove(cell_selected.id);
            cell_selected = null;
        });

        graph.cellRenderer.installCellOverlayListeners = function (state,
            overlay, shape) {
            mxEvent.addGestureListeners(shape.node, function (evt) {
                graph.connectionHandler.start(state, 1, 1);
                graph.isMouseDown = true;
                graph.isMouseTrigger = true;
                mxEvent.consume(evt);
            });
        };

        graph.addListener(mxEvent.CLICK, function (sender, evt) {
            var cell = evt.getProperty("cell"); // cell may be null
            if (cell != null) {
                cell_selected = cell;
                if (cell.style.includes('#0039ff')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'primary') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#6c757d')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'secondary') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#28a745')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'success') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#dc3545')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'danger') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#ffc107')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'warning') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#17a2b8')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'info') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#f8f9fa')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'light') {
                            $(this).prop("checked", true);
                        }
                    });
                }

                if (cell.style.includes('#343a40')) {
                    $('input[name^="btn_type"]').each(function () {
                        if ($(this).val() === 'dark') {
                            $(this).prop("checked", true);
                        }
                    });
                }
            }
            evt.consume();
        });

        graph.addListener(mxEvent.ADD_CELLS, function (sender, evt) {
            var cell = evt.getProperty("cells")[0];
            if (cell != null) {
                cell_selected = cell;

                if (cell.value === 'Button') {
                    cell_button_list.push(cell.id);
                } else if (cell.value === null) {
                    cell_card_list.push(cell.id);
                }

                $('input[name^="btn_type"]').each(function () {
                    if ($(this).val() === 'primary') {
                        $(this).prop("checked", true);
                    }
                });
            }
        });

        var tbContainer = document.getElementById('tool-bar');

        var toolbar = new mxToolbar(tbContainer);
        toolbar.enabled = false;

        function addToolbarItem(graph, toolbar, prototype, image) {
            var funct = function (graph, evt, cell, x, y) {
                graph.stopEditing(false);

                var vertex = graph.getModel().cloneCell(prototype);
                vertex.geometry.x = x;
                vertex.geometry.y = y;

                graph.addCell(vertex);
                graph.setSelectionCell(vertex);
            }

            var img = toolbar.addMode(null, image, function (evt, cell) {
                var pt = this.graph.getPointForEvent(evt);
                funct(graph, evt, cell, pt.x, pt.y);
            });

            mxEvent.addListener(img, 'mousedown', function (evt) {
                if (img.enabled == false) {
                    mxEvent.consume(evt);
                }
            });

            mxUtils.makeDraggable(img, graph, funct);
            return img;
        }

        var addVertex = function (icon, w, h, style, name) {
            if (name === 'Card') {
                name = null;
            }
            var vertex = new mxCell(name, new mxGeometry(0, 0, w, h), style);
            vertex.setVertex(true);

            if (name === null) {
                graph.getModel().beginUpdate();
                graph.insertVertex(vertex, null, 'Title', 1, 5, 80, 20, 'fontColor=#ffffff');
                graph.getModel().endUpdate();
            }

            var img = addToolbarItem(graph, toolbar, vertex, icon);
            img.enabled = true;

            graph.getSelectionModel().addListener(mxEvent.CHANGE, function () {
                var tmp = graph.isSelectionEmpty();
                mxUtils.setOpacity(img, (tmp) ? 100 : 20);
                img.enabled = tmp;
            });
        };

        let style_shape = 'fillColor=#0039ff;fontColor=white';
        let card_style = 'shape=image;image=./img/card-c.png;editable=0;';

        addVertex('./img/button.png', 100, 40, style_shape, 'Button');
        addVertex('./img/card.png', 500, 256, card_style, 'Card');

        var style = graph.getStylesheet().getDefaultVertexStyle();
        style[mxConstants.STYLE_PERIMETER] = mxPerimeter.RectanglePerimeter;
        style[mxConstants.STYLE_FONTSIZE] = 18;
        style[mxConstants.STYLE_STROKEWIDTH] = 0;
        style[mxConstants.STYLE_STROKECOLOR] = 'none';
        style[mxConstants.STYLE_FILLCOLOR] = 'none';
        style[mxConstants.STYLE_ROUNDED] = true;
        style[mxConstants.STYLE_IMAGE_ASPECT] = 0;

        $("input[name=btn_type]").on('click', function () {
            if ($(this).val() === 'primary') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#007bff;fontColor=white');
                }
            }

            if ($(this).val() === 'secondary') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#6c757d;fontColor=white');
                }
            }

            if ($(this).val() === 'success') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#28a745;fontColor=white');
                }
            }

            if ($(this).val() === 'danger') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#dc3545;fontColor=white');
                }
            }

            if ($(this).val() === 'warning') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#ffc107;fontColor=black');
                }
            }

            if ($(this).val() === 'info') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#17a2b8;fontColor=white');
                }
            }

            if ($(this).val() === 'light') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#f8f9fa;fontColor=#212529');
                }
            }

            if ($(this).val() === 'dark') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=#343a40;fontColor=#fff');
                }
            }

            if ($(this).val() === 'custom') {
                var color = $('#custom-color').val();
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=' + color + ';fontColor=#fff');
                }
            }
        })

        $("#export").click(function () {
            var outputSvg = graph.getSvg("#FFFFFF", 1, null, null, true, null, null);
            console.log(outputSvg);
        })

        $("#custom-color").on('change', function () {
            var color = $(this).val();
            var btnOption = $('input[name=btn_type]:checked').val();
            if (btnOption === 'custom') {
                if (cell_selected) {
                    graph.model.setStyle(cell_selected, 'fillColor=' + color + ';fontColor=#fff');
                }
            }
        });

        $("#show_header").on('change', function () {
            if ($(this)[0].checked) {
                if (cell_selected) {
                    if ($('#show_footer')[0].checked) {
                        graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-c.png');
                    } else {
                        graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-none-footer.png');
                    }
                }
            } else {
                if (cell_selected) {
                    if ($('#show_footer')[0].checked) {
                        graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-header.png');
                    } else {
                        graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-none.png');
                    }
                }
            }
        });

        $("#show_footer").on('change', function () {
            if ($(this)[0].checked) {
                if ($("#show_header")[0].checked) {
                    graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-c.png');
                } else {
                    graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-footer.png');
                }
            } else {
                if ($("#show_header")[0].checked) {
                    graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-none-footer.png');
                } else {
                    graph.model.setStyle(cell_selected, 'shape=image;image=./img/card-none.png');
                }
            }
        });

    }

    setInterval(function () {
        if (cell_selected != null) {
            if (cell_button_list.indexOf(cell_selected.id) > -1) {
                $("#option-button").show();
            } else {
                $("#option-button").hide();
            }

            if (cell_card_list.indexOf(cell_selected.id) > -1) {
                $("#option-card").show();
            } else {
                $("#option-card").hide();
            }
        } else {
            $("#option-button").hide();
        }
    }, 100);



    Array.prototype.remove = function () {
        var what, a = arguments, L = a.length, ax;
        while (L && this.length) {
            what = a[--L];
            while ((ax = this.indexOf(what)) !== -1) {
                this.splice(ax, 1);
            }
        }
        return this;
    };


};
